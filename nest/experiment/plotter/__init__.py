# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2019-2020 NITK Surathkal

"""Handle plotting results obtained"""

import matplotlib.style as style

# Set plot style
style.use('seaborn-paper')
style.use('ggplot')
